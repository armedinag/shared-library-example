/*

    Jenkinsfile_tools_library (Declarative Pipeline)

*/

pipeline {
    agent any

    parameters {
        string(name: 'GitLabURL', defaultValue: '', description: 'Url git clone repository')
        choice(name: 'Project', choices: 'skynet\nmeteoro\nalimarket\nbloomberg\nnitro\nnotifications-engine\nsuppliers-portal', description: 'Choose project')
        booleanParam(name: 'Publish', defaultValue: 'false', description: 'Do you want to publish on api manager?')
    }


    stages {

        stage('Example Username/Password') {
            environment {
                SERVICE_CREDS = 'my-predefined-username-password'
            }
            steps {
                sh 'echo "Service user is $SERVICE_CREDS"'

               // sh 'curl -u $SERVICE_CREDS https://myservice.example.com'
            }
        }

        stage('Clone the project') {
            steps {
                git 'https://github.com/eugenp/tutorials.git'
            }
        }

        stage("Compilation and Analysis") {
            failFast true
            parallel('Compilation') {
                stage('Installing') {
                    steps {
                        script {
                            if (isUnix()) {
                                sh "./mvnw clean install -DskipTests"
                            } else {
                                bat "./mvnw.cmd clean install -DskipTests"
                            }
                        }
                    }
                }
                stage('Branch B') {
                    agent {
                        label "for-branch-b"
                    }
                    steps {
                        echo "On Branch B"
                    }
                }
            }
        }

        stage('Parallel Stage') {
            when {
                branch 'master'
            }
            failFast true
            parallel {
                stage('Branch A') {
                    agent {
                        label "for-branch-a"
                    }
                    steps {
                        echo "On Branch A"
                    }
                }
                stage('Branch B') {
                    agent {
                        label "for-branch-b"
                    }
                    steps {
                        echo "On Branch B"
                    }
                }
                stage('Branch C') {
                    agent {
                        label "for-branch-c"
                    }
                    stages {
                        stage('Nested 1') {
                            steps {
                                echo "In stage Nested 1 within Branch C"
                            }
                        }
                        stage('Nested 2') {
                            steps {
                                echo "In stage Nested 2 within Branch C"
                            }
                        }
                    }
                }
            }
        }

        stage("Tests and Deployment") {
            steps {
                echo 'This stage will be executed first.'
            }
        }
    }


}