/*

    Jenkinsfile_tools_library (Declarative Pipeline)

*/

pipeline {
    agent any

    parameters {
        string(name: 'GitLabURL', defaultValue: 'https://gitlab.com/armedinag/simple-java-maven-app.git', description: 'Url git clone repository')
        choice(name: 'Project', choices: 'suppliers-portal\nskynet\nmeteoro\nalimarket\nbloomberg\nnitro\nnotifications-engine', description: 'Choose project')
        booleanParam(name: 'Publish', defaultValue: 'false', description: 'Do you want to publish on api manager?')
    }

    environment {
        //Environment = ""
        AlicorpProject = ""
        Publish = ""
        base_image = ""
    }


    stages {

        stage('Release Candidate') {
            steps {
                script {
                    env.Environment = "prod"
                    env.AlicorpProject = "${params.Project}"
                    env.Publish = "${params.Publish}"
                    env.RC = ""
                    base_image = ""
                }

                cleanWs()
                checkout scm

                dir('temp-repo') {
                    git branch: "main",
                            url: params.GitLabURL,
                            credentialsId: 'd39b8ad0-1bee-4ac0-9b85-4565e04f88f8'
                }

                echo 'Release Candidate'
            }
        }

        stage('Start Release') {
            steps {
                echo 'Start Release'
            }
        }

        stage('Promote image') {
            steps {
                echo 'Promote image'
            }
        }

        /* stage('Build & U.Test') {
           utils.testMaven()
         }

         stage('QA Analisys') {
           utils.executeSonar(params.Project)
         }

         stage('Upload Artifact to Nexus') {
           utils.buildMaven()
         }

         stage('Upload to Container Registry') {
            utils.UploadDokcerRegistry()
         }
         */

        stage('Deploy to AKS') {
            steps {
                echo 'Deploy to AKS'
            }
        }

        stage('Publish to Api Manager') {
            steps {
                echo 'Publish to Api Manager'
            }
        }

        stage('Post Execution') {
            steps {
                echo 'Post Execution'
            }
        }

    }
}


