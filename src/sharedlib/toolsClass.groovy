package sharedlib

import org.jenkinsci.plugins.docker.workflow.*;

class toolsClass implements Serializable {


/*
  * Get information from the main pipeline
  */
    def steps
    def script
    def type

    toolsClass(steps, script, String type = '') {

        this.steps = steps
        this.script = script
        steps.echo "steps: ${steps}"
        steps.echo "script: ${script}"
        this.type = type
    }

    def sayHello(name) {
        steps.echo "Hello, ${name}."
    }
}