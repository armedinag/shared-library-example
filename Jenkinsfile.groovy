/* Best Practices using Jenkins Pipeline:
    Use the genuine Jenkins Pipeline
    Develop your pipeline as code
    Any non-setup work in your pipeline should occur within a stage block.
    Any material work in a pipeline must be performed within a node block.
    Don’t use input within a node block.
    Never set environment variables with env global variable
    Wrap your inputs in a timeout
 */

/*

    Jenkinsfile_tools_library (Declarative Pipeline)

 */

@Library('pipeline-library-demo') _

//import sharedlib.tools

//evenOrOdd(currentBuild.getNumber()) /*declarative pipeline*/

def toolsClass = new sharedlib.toolsClass(steps, this)
def hello = new sharedlib.example()


pipeline {
    agent any
    /*
    tools {
        maven 'MAVEN_PATH'
    }*/
    environment {
        DEPLOY_TO = 'production'
        //AWS_ACCESS = credentials('AWS_ KEY')
        //AWS_SECRET = credentials('AWS_SECRET')
    }
    parameters {
        string(name: 'NAME', description: 'Please tell me your name?')

        text(name: 'DESC', description: 'Describe about the job details')

        booleanParam(name: 'SKIP_TEST', description: 'Want to skip running Test cases?')

        choice(name: 'BRANCH', choices: ['Master', 'Dev'], description: 'Choose branch')

        password(name: 'SONAR_SERVER_PWD', description: 'Enter SONAR password')
    }
    stages {
        /*
        stage('Sanity check') {
            steps {
                input "Does the staging environment look ok?"
            }
        }*/
        stage('Initialization') {
            steps {
                echo "${DEPLOY_TO}"
            }
        }
        stage('Load Tools') {
            steps {
                sh "mvn -version"
            }
        }
        /*
        stage('Parallel Stage') {
            parallel {
                stage('windows script') {
                    agent {
                        label "windows"
                    }
                    steps {
                        echo "Running in windows agent"
                        bat 'echo %PATH%'
                    }
                }
                stage('linux script') {
                    agent {
                        label "linux"
                    }
                    steps {
                        sh "Running in Linux agent"
                    }
                }
            }
        }
        */
        stage('Welcome Step') {
            /*
            when {
                allOf {
                    not { branch 'master' }
                    environment name: 'JOB_NAME', value: 'Foo'
                }
            }
            when {
                anyOf {
                    branch 'master';
                    branch 'staging'
                }
            }
            when {
                allOf {
                    branch 'master';
                    environment name: 'DEPLOY_TO', value: 'production'
                }
            }*/
            when {
                environment name: 'DEPLOY_TO', value: 'production'
            }
            steps {
                echo 'Welcome to LambdaTest'
            }
        }
        stage('Printing Parameters') {
            steps {
                echo "Hello ${params.NAME}"

                echo "Job Details: ${params.DESC}"

                echo "Skip Running Test case ?: ${params.SKIP_TEST}"

                echo "Branch Choice: ${params.BRANCH}"

                echo "SONAR Password: ${params.SONAR_SERVER_PWD}"
            }
        }
        stage('build') {
            when {
                expression {
                    return params.BRANCH == 'Dev';
                }
            }
            /*
            when {
                expression {
                    return env.BRANCH_NAME == 'dev';
                }
            }
            when {
                branch 'dev'
            }
            */
            steps {
                echo "Working on dev branch"
            }
        }
        stage('Demo') {
            steps {
                echo 'Hello world'
                sayHello 'Joe'
                sayHello() /* invoke with default arguments */
                script {
                    log.info 'Starting'
                    log.warning 'Nothing to do!'
                    hello.sayHello('Alvaro')
                    toolsClass.sayHello('Alvaro')

                    echo 'The value of foo is : ' + GlobalVars.foo

                    def person = new sharedlib.SampleClass()
                    person.age = 21
                    person.increaseAge(10)
                    echo 'Incremented age, is now : ' + person.age

                }
            }
        }
        stage('Build and Publish Image') {
            when {
                branch 'master'    //only run these steps on the master branch
            }
            steps {
                sh """
                    docker build -t ${IMAGE} .
                    docker tag ${IMAGE} ${IMAGE}:${VERSION}
                    docker push ${IMAGE}:${VERSION}
                """
            }
        }
        stage('Test Firefox') {
            parallel {
                stage('Test Firefox') {
                    steps {
                        echo 'Testing Firefox'
                    }
                }
                stage('Test Chrome') {
                    steps {
                        echo 'Testing Chrome'
                    }
                }
                stage('Test Edge') {
                    steps {
                        echo 'Testing Edge'
                    }
                }
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploy'
            }
        }
    }
    post {
        always {
            echo "You can always see me"
        }
        success {
            echo "I am running because the job ran successfully"
            /*
            slackSend channel:'#ops-room',
                    color: 'good',
                    message: 'Completed successfully.'
            */
        }
        unstable {
            echo "Gear up ! The build is unstable. Try fix it"
        }
        failure {
            echo "OMG ! The build failed"
            /*
            mail to: 'team@example.com',
                    subject: 'Failed Pipeline',
                    body: "Something is wrong"*/
        }
    }
}
